﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EDMI_Test
{
    // YourImplementationOfMyInterface "implements" the IMyInterface interface
    class YourImplementationOfMyInterface : IMyInterface
    {
        public IEnumerable<Tuple<string, int>> GetWordCounts(string[] words)
        {
            // store data in dictionary
            Dictionary<string, int> wordCounts = words.GroupBy(x => x)
                                                    .ToDictionary(y => y.Key,
                                                    z => z.Count());

            return wordCounts.Select(x => Tuple.Create(x.Key, x.Value)).ToList();
        }

        public IEnumerable<Tuple<string, int>> GetWordCounts(string commaDelimitedWords)
        {
            // transform comma delimited to array
            string[] words = commaDelimitedWords.Split(',')
                                                .Select(x => x.Trim())
                                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                                .ToArray();

            // we can call previous method
            return this.GetWordCounts(words);
        }

        public bool IsBitSet(int value, int bit)
        {
            return (value & (1 << bit)) != 0;
        }

        public IEnumerable<int> GetBitsSet(int value)
        {
            List<int> result = new List<int>();

            int index = 0; // start at bit index 0

            // for positive number
            if (value > 0)
            {
                // If the number is 0, no bits are set
                while (value != 0)
                {
                    // check if the bit at the current index 0 is set
                    if ((value & 1) == 1)
                    {
                        result.Add(index);
                    }

                    // shift all bits one position to the right
                    value = value >> 1;

                    // mark as next index
                    index = index + 1;
                }
            }

            // for negative number
            // we try find all not set bits then reverse it
            if (value < 0)
            {
                // for value = -1, all bits (32) are set
                if (value == -1)
                {
                    result = Enumerable.Range(0, 32).ToList();
                }
                else
                {
                    // temporary list for reverse set
                    List<int> temp = new List<int>();

                    // If the number is -1, all bits are set
                    while (value != -1)
                    {
                        // check if the bit at the current index 0 is not set
                        if ((value & 1) == 0)
                        {
                            temp.Add(index);
                        }

                        // shift all bits one position to the right
                        value = value >> 1;

                        // mark as next index
                        index = index + 1;
                    }

                    result = Enumerable.Range(0, 32).Except(temp).ToList();
                }
            }

            return result;
        }

        public int SetBits(int value, int[] bitsToSet)
        {
            for (int i = 0; i < bitsToSet.Length; ++i)
            {
                value = value |= (1 << bitsToSet[i]);
            }

            return value;
        }
    }
}
